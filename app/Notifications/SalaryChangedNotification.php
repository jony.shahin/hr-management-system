<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SalaryChangedNotification extends Notification
{
    use Queueable;

    public $employee;
    public $newSalary;

    /**
     * Create a new notification instance.
     */
    public function __construct($employee, $newSalary)
    {
        $this->employee = $employee;
        $this->newSalary = $newSalary;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
            ->subject('Your Salary has Changed')
            ->line('Dear ' . $this->employee->name . ',')
            ->line('Your salary has been updated. Your new salary is: ' . $this->newSalary)
            ->action('View Details', url('/employee/' . $this->employee->id))
            ->line('Thank you for being a valuable employee.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
