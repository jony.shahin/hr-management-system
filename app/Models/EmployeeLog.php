<?php

namespace App\Models;

use App\Events\EmployeeCreated;
use App\Events\EmployeeDeleting;
use App\Events\EmployeeUpdating;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'action', 'employee_id', 'user_id', 'old_data', 'new_data',
    ];

    protected $dispatchesEvents = [
        'created' => EmployeeCreated::class,
        'updating' => EmployeeUpdating::class,
        'deleting' => EmployeeDeleting::class,
    ];
}
