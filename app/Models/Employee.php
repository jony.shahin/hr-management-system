<?php

namespace App\Models;

use App\Events\SalaryChanged;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function manager()
    {
        return $this->belongsTo(Employee::class, 'manager_id');
    }

    public function subordinates()
    {
        return $this->hasMany(Employee::class, 'manager_id');
    }

    public function isFounder()
    {
        return $this->is_founder;
    }

    public function updateSalary($newSalary)
    {
        if ($this->salary !== $newSalary) {
            event(new SalaryChanged($this, $newSalary));
            $this->salary = $newSalary;
            $this->save();
        }
    }

    public function employeeJobs()
    {
        return $this->hasMany(EmployeeJob::class, 'employee_id');
    }
}
