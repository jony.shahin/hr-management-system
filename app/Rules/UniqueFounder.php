<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Employee;

class UniqueFounder implements Rule
{
    public function passes($attribute, $value)
    {
        // Check if there is already a founder in the database
        if ($value) {
            return !Employee::where('is_founder', true)->exists();
        } else {
            return true;
        }
    }

    public function message()
    {
        return 'There can only be one founder in the company.';
    }
}
