<?php

namespace App\Listeners;

use App\Events\EmployeeDeleting;
use App\Models\EmployeeLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class LogEmployeeDeleting
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(EmployeeDeleting $event): void
    {
        $employee = $event->employee;

        EmployeeLog::create([
            'action' => 'deleted',
            'employee_id' => $employee->id,
            'user_id' => auth()->user()->id,
            'new_data' => $employee->toArray(),
        ]);

        Log::channel('employee')->info('Employee deleted: ', ['employee_id' => $employee->id]);
    }
}
