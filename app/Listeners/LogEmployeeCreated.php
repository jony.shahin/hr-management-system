<?php

namespace App\Listeners;

use App\Events\EmployeeCreated;
use App\Models\EmployeeLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class LogEmployeeCreated
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(EmployeeCreated $event): void
    {
        $employee = $event->employee;


        EmployeeLog::create([
            'action' => 'created',
            'employee_id' => $employee->id,
            'user_id' => auth()->user()->id,
            'new_data' => $employee->toArray(),
        ]);

        Log::channel('employee')->info('Employee created: ', ['employee_id' => $employee->id]);
    }
}
