<?php

namespace App\Listeners;

use App\Events\EmployeeUpdating;
use App\Models\EmployeeLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class LogEmployeeUpdating
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(EmployeeUpdating $event): void
    {
        $employee = $event->employee;

        EmployeeLog::create([
            'action' => 'updated',
            'employee_id' => $employee->id,
            'user_id' => auth()->user()->id,
            'new_data' => $employee->toArray(),
        ]);

        Log::channel('employee')->info('Employee updated: ', ['employee_id' => $employee->id]);
    }
}
