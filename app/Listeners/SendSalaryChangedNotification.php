<?php

namespace App\Listeners;

use App\Events\SalaryChanged;
use App\Notifications\SalaryChangedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendSalaryChangedNotification
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(SalaryChanged $event): void
    {
        $event->employee->notify(new SalaryChangedNotification($event->employee, $event->newSalary));
    }
}
