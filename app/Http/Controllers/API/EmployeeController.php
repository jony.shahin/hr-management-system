<?php

namespace App\Http\Controllers\API;

use App\Exports\EmployyesExport;
use App\Http\Controllers\Controller;
use App\Imports\EmployeesImport;
use App\Models\Employee;
use App\Rules\UniqueFounder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'page' => 'integer|nullable',
                    'per_page' => 'integer|nullable',
                    'search' => 'string|nullable',
                ]
            );

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $per_page = $request->input('per_page', 10);

            $employees  = Employee::query();

            if ($request->filled('search')) {
                $search = $request->input('search');
                $employees->where('name', 'like', '%' . $search . '%')
                    ->orWhere('salary', 'LIKE', '%' . $search . '%');
            }
            $employees->with(['manager']);

            if ($per_page == 0) {
                $fetch_data['employees'] = $employees->get();
            } else {
                $fetch_data['employees'] = $employees->paginate($per_page);
            }

            return success_response($fetch_data, __('Employee List retrieved successfully'));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function show(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'employee_id' => 'required|integer'
                ]
            );

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $employee = Employee::where('id', $request->employee_id)
                ->with(['manager'])
                ->first();
            if ($employee == null) {
                return notfound_error();
            }
            $fetch_data['employee'] = $employee;

            return success_response($fetch_data, __('Employee details retrieved successfully'));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'name' => 'required',
                    'email' => 'required|email|unique:employees',
                    'age' => 'required|integer',
                    'hire_date' => 'required|date',
                    'salary' => 'required|numeric',
                    'gender' => 'required|string',
                    'job_title' => 'required|string',
                    'is_founder' => ['required', new UniqueFounder],
                    'manager_id' => 'integer',
                ]
            );
            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            if (!Employee::where('is_founder', true)->exists()) {
                $data['is_founder'] = true;
            }

            if (!$request->is_founder && !$request->has('manager_id')) {
                return error_response('Employee should have a manager');
            }

            $employee = Employee::create($data);

            return success_response($employee, 'Employee Created Successfully');
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function update(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'employee_id' => 'required|integer',
                    'name' => 'string',
                    'email' => 'email|unique:employees,email, ' . $request->employee_id,
                    'age' => 'integer',
                    'hire_date' => 'date',
                    'salary' => 'numeric',
                    'gender' => 'string',
                    'job_title' => 'string',
                    'is_founder' => ['required', new UniqueFounder],
                    'manager_id' => 'integer',
                ]
            );
            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $employee = Employee::findOrFail($request->employee_id);

            if ($employee->isFounder() && !$data['is_founder']) {
                // If the employee was a founder and is being updated to a non-founder,
                // make sure there is at least one other founder in the database
                if (!Employee::where('is_founder', true)->where('id', '!=', $request->employee_id)->exists()) {
                    return error_response('There must be at least one founder.');
                }
            }

            $employee->update($data);

            if ($request->filled('salary') && $request->salary != $employee->salary) {
                $employee->updateSalary($request->salary);
            }

            return success_response($employee, 'Employee Updated Successfully');
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function destroy(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'employee_id' => 'required|integer',
                ]
            );
            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $employee = Employee::findOrFail($request->employee_id);

            if ($employee->isFounder()) {
                return error_response('The founder cannot be deleted.');
            }

            $employee->delete();

            return success_response([], 'Employee Deleted Successfully');
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function getManagers(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'employee_id' => 'required|integer'
                ]
            );

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $employee = Employee::where('id', $request->employee_id)->first();

            if ($employee == null) {
                return notfound_error();
            }

            $managers[] = $employee->name;

            while ($employee->manager) {
                $managers[] = $employee->manager->name;
                $employee = $employee->manager;
            }
            $fetch_data['managers'] = $managers;

            return success_response($fetch_data, __('Employee managers retrieved successfully'));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function getManagersWithSalaries(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'employee_id' => 'required|integer'
                ]
            );

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $employee = Employee::where('id', $request->employee_id)->first();

            if ($employee == null) {
                return notfound_error();
            }

            $managers[] = $employee->name . ': ' . $employee->salary;

            while ($employee->manager) {
                $managers[] = $employee->manager->name . ': ' . $employee->manager->salary;
                $employee = $employee->manager;
            }
            $fetch_data['managers'] = $managers;

            return success_response($fetch_data, __('Employee managers retrieved successfully'));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function import(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'file' => 'required'
                ]
            );

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $file = $request->file('file');

            (new EmployeesImport)->import($file, null, \Maatwebsite\Excel\Excel::CSV);
            return success_response([], __('CSV file imported successfully'));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function export()
    {
        try {
            return Excel::download(new EmployyesExport, 'employees.csv', \Maatwebsite\Excel\Excel::CSV);
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }
}
