<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\EmployeeJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeJobController extends Controller
{
    public function index(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'page' => 'integer|nullable',
                    'per_page' => 'integer|nullable',
                    'search' => 'string|nullable',
                ]
            );

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $per_page = $request->input('per_page', 10);

            $employeeJobs  = EmployeeJob::query();

            if ($request->filled('search')) {
                $search = $request->input('search');
                $employeeJobs->where('title', 'like', '%' . $search . '%');
            }
            $employeeJobs->with(['employee']);

            if ($per_page == 0) {
                $fetch_data['employeeJobs'] = $employeeJobs->get();
            } else {
                $fetch_data['employeeJobs'] = $employeeJobs->paginate($per_page);
            }

            return success_response($fetch_data, __('Employee Job List retrieved successfully'));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function show(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'employee_job_id' => 'required|integer'
                ]
            );

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $employee_job = EmployeeJob::where('id', $request->employee_job_id)
                ->with(['employee'])
                ->first();
            if ($employee_job == null) {
                return notfound_error();
            }
            $fetch_data['employee_job'] = $employee_job;

            return success_response($fetch_data, __('Employee Job details retrieved successfully'));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'employee_id' => 'required|integer',
                    'title' => 'required|string'
                ]
            );
            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $employee_job = EmployeeJob::create($data);

            $employee_job = EmployeeJob::where('id', $employee_job->id)->with('employee')->first();

            return success_response($employee_job, 'Employee Created Successfully');
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function update(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'employee_job_id' => 'required|integer',
                    'employee_id' => 'required|integer',
                    'title' => 'required|string',
                ]
            );
            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $employee_job = EmployeeJob::findOrFail($request->employee_job_id);

            if ($employee_job == null) {
                return notfound_error();
            }

            $employee_job->update($data);

            $employee_job = EmployeeJob::where('id', $request->employee_job_id)->with('employee')->first();

            return success_response($employee_job, 'Employee Job Updated Successfully');
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function destroy(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'employee_job_id' => 'required|integer',
                ]
            );
            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $employee_job = EmployeeJob::findOrFail($request->employee_job_id);

            $employee_job->delete();

            return success_response([], 'Employee Job Deleted Successfully');
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }
}
