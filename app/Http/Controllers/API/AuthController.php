<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\VerificationMail;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function userRegister(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'name' => 'required',
                    'email' => 'required|email|unique:users',
                    'phone' => 'required|unique:users',
                    'password' => 'required|confirmed|min:8',
                ]
            );

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $current_time = Carbon::now()->timestamp;
            $otp = rand(100000, 999999);
            $insert_data = new User();
            $insert_data->name = $request->name;
            $insert_data->email = $request->email;
            $insert_data->phone = $request->phone;
            $insert_data->password = Hash::make($request->password);
            $insert_data->otp = $otp;
            $insert_data->otp_created_at = date('Y-m-d H:i:s', strtotime('+15 minutes'));
            $insert_data->save();

            $insert_data->assignRole('user');

            /*************** Send email code***********************/
            $details = [
                'title' => 'OTP Code',
                'body' => 'Hi, your OTP code is: ' . $otp . '. It\'s valid for 15 minutes.'
            ];

            Mail::to($request->email)->send(new VerificationMail($details));
            /*************** End Send email code***********************/

            $fetch_user = User::where('id', $insert_data->id)->first();

            $fetch_data['user_data'] = $fetch_user;

            return success_response($fetch_data, __("Register Successfully"));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function verifyToken()
    {
        try {
            $user = Auth::user();
            $user = User::find($user->id);

            if ($user == null) {
                return notfound_error();
            }

            if ($user->email_verified_at == null) {
                return error_response(__("User's Email not verified"));
            }

            $user = User::find($user->id);

            return success_response($user, __("User Verified"));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function login(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'email' => 'required|email',
                    'password' => 'required',
                ]
            );
            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $user = User::where('email', $request->email)->first();

            if ($user == null) {
                return notfound_error();
            }

            if ($user->email_verified_at == null) {
                return error_response(__("User's Email not verified"));
            }

            if ($user->email_verified_at != null) {
                if (!Auth::attempt($request->only(['email', 'password']))) {
                    return error_response(__("Email & Password does not match with our record."));
                }

                $fetch_data['user_data'] = $user;
                $fetch_data['token'] = $user->createToken("my-app-token")->plainTextToken;

                return success_response($fetch_data, __("User Logged In Successfully"));
            }

            return error_response(__("Something went wrong"));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function changePassword(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'oldPassword' => 'required',
                'password' => 'required|min:8|confirmed',
            ]);

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $user = Auth::user();
            $user_data = User::find($user->id);

            if ($user_data == null) {
                return notfound_error();
            }

            if (Hash::check($request->oldPassword, $user_data->password)) {
                $user_data->password = Hash::make($request->password);
                $user_data->save();

                $user =  User::where('id', $user->id)->first();
                $fetch_data['user_data'] = $user;

                return success_response($fetch_data, __("Password Changed Successfully"));
            } else {
                return error_response(__("Password is incorrect"));
            }
        } catch (Exception $e) {
            return catch_error($e->getMessage());
        }
    }

    public function deleteUser()
    {
        try {
            $user = Auth::user();
            $user = User::find($user->id);
            if ($user == null) {
                return notfound_error();
            }
            $user->delete();
            $user->tokens()->delete();
            return success_response([], __("User Deleted Successfully"));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function logout(Request $request)
    {
        try {
            $user = Auth::user();
            $user = User::find($user->id);
            if ($user == null) {
                return notfound_error();
            }
            $user->tokens()->delete();
            return success_response([], __("User Logout Successfully"));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function sendVerificationCode(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'email' => 'required|email',
                ]
            );

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            // Send email code
            $current_time = Carbon::now()->timestamp;
            $user = User::where('email', $request->email)->first();

            if ($user == null) {
                return notfound_error();
            }

            if ($user->email_verified_at != null) {
                $user = User::where('email', $request->email)->first();
                $fetch_data['user_data'] = $user;
                return error_response(__("User's Email already verified"));
            }

            $otp = rand(100000, 999999);
            $user->otp = $otp;
            $user->otp_created_at = date('Y-m-d H:i:s', strtotime('+15 minutes'));
            $user->save();

            /*************** Send email code***********************/
            $details = [
                'title' => 'OTP Code',
                'body' => 'Hi, your OTP code is: ' . $otp . '. It\'s valid for 15 minutes.'
            ];

            Mail::to($request->email)->send(new VerificationMail($details));
            /*************** End Send email code***********************/

            $user = User::where('email', $request->email)->first();
            $fetch_data['user_data'] = $user;

            return success_response($fetch_data, __("Verfication Code Sent Successfully"));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function verify_otp(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'code' => 'required|digits:6',
                'email' => 'required|email',
            ]);

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $current = date('Y-m-d H:i:s');
            $user_data = User::where('email', $request->email)->first();

            if ($user_data == null) {
                return error_response(__("User not found"));
            }

            if ($user_data->email_verified_at != null) {
                $user = User::where('email', $request->email)->first();
                $fetch_data['user_data'] = $user;
                return error_response(__("User's Email already verified"));
            }

            if ($user_data->otp != $request->code) {
                return error_response(__("Invalid OTP"));
            }

            if (strtotime($current) > strtotime($user_data->otp_created_at)) {
                return error_response(__("Otp is expired"));
            }

            if ($request->code == $user_data->otp) {
                User::where('email', $request->email)->update([
                    'email_verified_at' => date('Y-m-d H:i:s')
                ]);

                $user =  User::where('email', $request->email)->first();
                $token =  $user->createToken('my-app-token')->plainTextToken;
                $fetch_data['token'] = $token;
                $fetch_data['user_data'] = $user;

                return success_response($fetch_data, __("Email verified successfully"));
            } else {
                return error_response(__("Invalid OTP"));
            }
        } catch (Exception $e) {
            return catch_error($e->getMessage());
        }
    }

    public function forgotPassword(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make(
                $data,
                [
                    'email' => 'required|email',
                ]
            );

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            // Send email code
            $current_time = Carbon::now()->timestamp;
            $user = User::where('email', $request->email)->first();

            if ($user == null) {
                return notfound_error();
            }

            $otp = rand(100000, 999999);
            $user->otp = $otp;
            $user->otp_created_at = date('Y-m-d H:i:s', strtotime('+15 minutes'));
            $user->save();

            /*************** Send email code***********************/
            $details = [
                'title' => 'OTP Code',
                'body' => 'Hi, your OTP code is: ' . $otp . '. It\'s valid for 15 minutes.'
            ];

            Mail::to($request->email)->send(new VerificationMail($details));
            /*************** End Send email code***********************/

            $fetch_data = [];

            return success_response($fetch_data, __("Verfication Code Sent to your Email"));
        } catch (\Throwable $th) {
            return catch_error($th->getMessage());
        }
    }

    public function resetPassword(Request $request)
    {
        try {
            $data = $request->all();
            $validator = Validator::make($data, [
                'code' => 'required|digits:6',
                'email' => 'required|email',
                'password' => 'required|min:8|confirmed',
            ]);

            if ($validator->fails()) {
                return validation_error($validator->messages()->all());
            }

            $current = date('Y-m-d H:i:s');
            $user_data = User::where('email', $request->email)->first();

            if ($user_data == null) {
                return notfound_error();
            }

            if ($user_data->otp != $request->code) {
                return error_response(__("OTP is not match"));
            }

            if (strtotime($current) > strtotime($user_data->otp_created_at)) {
                return error_response(__("Otp is expired"));
            }

            if ($request->code == $user_data->otp) {
                $user_data->password = Hash::make($request->password);
                $user_data->save();

                $user =  User::where('email', $request->email)->first();
                $token =  $user->createToken('my-app-token')->plainTextToken;
                $fetch_data['token'] = $token;
                $fetch_data['user_data'] = $user;

                return success_response($fetch_data, __("Password Changed Successfully"));
            } else {
                return error_response(__("Invalid OTP"));
            }
        } catch (Exception $e) {
            return catch_error($e->getMessage());
        }
    }
}
