<?php

use Illuminate\Http\Response;

function validation_error($message)
{
    return response()->json([
        "message" => $message[0],
        "code" => Response::HTTP_BAD_REQUEST,
        "data" => null,
        "response" => 'error'
    ], Response::HTTP_BAD_REQUEST);
}

function unauthorized_error()
{
    return response()->json([
        "message" => __('User Unauthorized'),
        "code" => Response::HTTP_UNAUTHORIZED,
        "data" => null,
        "response" => 'error'
    ], Response::HTTP_UNAUTHORIZED);
}

function notfound_error()
{
    return response()->json([
        "message" => 'Not Found',
        "code" => Response::HTTP_NOT_FOUND,
        "data" => null,
        "response" => 'error'
    ], Response::HTTP_NOT_FOUND);
}

function success_response($data, $message)
{
    return response()->json([
        'message' => $message,
        'code' => 1,
        'data' => $data,
        "response" => 'success'
    ], Response::HTTP_OK);
}

function catch_error($message)
{
    return response()->json([
        'message' => $message,
        'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
        'data' => null,
        'response' => 'error'
    ], Response::HTTP_INTERNAL_SERVER_ERROR);
}

function error_response($message)
{
    return response()->json([
        'message' => $message,
        'code' => Response::HTTP_PAYMENT_REQUIRED,
        'data' => null,
        'response' => 'error'
    ], Response::HTTP_PAYMENT_REQUIRED);
}
