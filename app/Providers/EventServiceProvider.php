<?php

namespace App\Providers;

use App\Events\EmployeeCreated;
use App\Events\EmployeeDeleting;
use App\Events\EmployeeUpdating;
use App\Events\SalaryChanged;
use App\Listeners\LogEmployeeCreated;
use App\Listeners\LogEmployeeDeleting;
use App\Listeners\LogEmployeeUpdating;
use App\Listeners\SendSalaryChangedNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        SalaryChanged::class => [
            SendSalaryChangedNotification::class,
        ],
        EmployeeCreated::class => [
            LogEmployeeCreated::class,
        ],
        EmployeeUpdating::class => [
            LogEmployeeUpdating::class,
        ],
        EmployeeDeleting::class => [
            LogEmployeeDeleting::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
