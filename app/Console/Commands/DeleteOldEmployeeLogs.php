<?php

namespace App\Console\Commands;

use App\Models\EmployeeLog;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteOldEmployeeLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:delete-old-employee-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete logs older than 1 month from the employee logs table';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $oneMonthAgo = Carbon::now()->subMonth();

        $employee_logs = EmployeeLog::query();
        $employee_logs = $employee_logs->whereDate('created_at', '<', $oneMonthAgo)->get();
        $employee_logs->delete();

        $this->info('Old employee logs deleted successfully.');
    }
}
