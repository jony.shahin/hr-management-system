<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class RemoveAllLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:remove-all-logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all log files';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $logPath = storage_path('logs');

        if (File::isDirectory($logPath)) {
            File::cleanDirectory($logPath);
            $this->info('All log files removed successfully.');
        } else {
            $this->warn('Log directory not found.');
        }
    }
}
