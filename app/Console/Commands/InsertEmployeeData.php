<?php

namespace App\Console\Commands;

use App\Models\Employee;
use Illuminate\Console\Command;

class InsertEmployeeData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employees:insert {count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert employee data with a progress bar';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $count = (int)$this->argument('count');

        $this->info("Inserting $count employee records...");

        $bar = $this->output->createProgressBar($count);

        for ($i = 1; $i <= $count; $i++) {
            Employee::create([
                'name' => "Employee $i",
                'email' => "employee$i@gmail.com",
                'age' => rand(20, 60),
                'hire_date' => now(),
                'salary' => rand(500000, 3000000),
                'gender' => rand(0, 1) ? 'Male' : 'Female',
                'job_title' => 'Employee',
                'is_founder' => false,
                'manager_id' => null,
            ]);

            $bar->advance();
        }

        $bar->finish();
        $this->line('');
        $this->info("Successfully inserted $count employee records.");
    }
}
