<?php

namespace App\Console\Commands;

use App\Models\Employee;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ExportEmployeesToJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employees:export-json {filename?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export all employees to a JSON file';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $filename = $this->argument('filename') ?? 'employees.json';

        $employees = Employee::all();

        $data = $employees->map(function ($employee) {
            return $employee->toArray();
        });

        $json = json_encode($data, JSON_PRETTY_PRINT);

        Storage::put($filename, $json);

        $this->info("Employees exported to $filename successfully.");
    }
}
