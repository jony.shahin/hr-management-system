<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

class ExportDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:export {filename?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export the database to a SQL file';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $filename = $this->argument('filename') ?? 'database_backup.sql';

        $host = Config::get('database.connections.mysql.host');
        $database = Config::get('database.connections.mysql.database');
        $username = Config::get('database.connections.mysql.username');
        $password = Config::get('database.connections.mysql.password');

        $command = sprintf(
            'mysqldump -h %s -u %s -p%s %s > %s',
            $host,
            $username,
            $password,
            $database,
            $filename
        );

        $this->info('Exporting database to ' . $filename);
        exec($command);

        $this->info('Database exported successfully.');
    }
}
