<?php

namespace App\Imports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;

class EmployeesImport implements ToModel
{
    use Importable;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Employee([
            'name' => $row[0],
            'email' => $row[1],
            'age' => $row[2],
            'hire_date' => $row[3],
            'salary' => $row[4],
            'gender' => $row[5],
            'job_title' => $row[6],
            'is_founder' => $row[7],
            'manager_id' => $row[8],
        ]);
    }
}
