<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\EmployeeController;
use App\Http\Controllers\API\EmployeeJobController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('throttle:10,1')->group(function () {

    Route::prefix('register')->group(function () {
        Route::post('/user', [AuthController::class, 'userRegister']);
    });

    Route::prefix('user')->group(function () {
        Route::middleware('auth:sanctum')->group(function () {
            Route::group(['middleware' => ['role:user']], function () {
                Route::get('/verify-token', [AuthController::class, 'verifyToken']);
                Route::post('/change-password', [AuthController::class, 'changePassword']);
                Route::get('/delete-user', [AuthController::class, 'deleteUser']);
                Route::get('/logout', [AuthController::class, 'logout']);
            });
        });
    });

    Route::prefix('email')->group(function () {
        Route::post('/send-verification-code', [AuthController::class, 'sendVerificationCode']);
        Route::post('/verify', [AuthController::class, 'verify_otp']);
    });

    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/forgot-password', [AuthController::class, 'forgotPassword']);
    Route::post('/reset-password', [AuthController::class, 'resetPassword']);

    Route::prefix('admin')->group(function () {
        Route::post('/login', [AuthController::class, 'login']);
        Route::middleware('auth:sanctum')->group(function () {
            Route::group(['middleware' => ['role:admin']], function () {
                Route::get('/logout', [AuthController::class, 'logout']);
                Route::prefix('employee')->group(function () {
                    Route::post('/all', [EmployeeController::class, 'index']);
                    Route::post('/details', [EmployeeController::class, 'show']);
                    Route::post('/add', [EmployeeController::class, 'store']);
                    Route::post('/update', [EmployeeController::class, 'update']);
                    Route::post('/delete', [EmployeeController::class, 'destroy']);
                    Route::post('/managers', [EmployeeController::class, 'getManagers']);
                    Route::post('/managers-with-salary', [EmployeeController::class, 'getManagersWithSalaries']);
                    Route::post('/import', [EmployeeController::class, 'import']);
                    Route::get('/export', [EmployeeController::class, 'export']);
                });
                Route::prefix('employee-job')->group(function () {
                    Route::post('/all', [EmployeeJobController::class, 'index']);
                    Route::post('/details', [EmployeeJobController::class, 'show']);
                    Route::post('/add', [EmployeeJobController::class, 'store']);
                    Route::post('/update', [EmployeeJobController::class, 'update']);
                    Route::post('/delete', [EmployeeJobController::class, 'destroy']);
                });
            });
        });
    });
});
